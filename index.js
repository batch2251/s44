/*
	FETCH METHOD

	- a FETCH METHOD in javascript is used to request data from a server. The request can be of any type of API that returns the data in JSON or XML

	Sample fetching:

		fetch ("https://jsonplaceholder.typicode.com/posts")
		- api for the get request
*/
let posts = [];

const getPosts = fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json() )
.then((data) => {
	let postEntries = "";
	posts = data
	showPosts(data)
});

const showPosts = (data) => {
	let postEntries = ""
	data.forEach((post) => {
		postEntries += `<div style="border-bottom: 1px solid red" id="post-${post.id}"> 
		<h3 id="post-title-${post.id}">${post.title} <h3/>
		<h4 id="post-userId-${post.id}"> userId: ${post.userId}</h4>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onClick="editPost(${post.id})">Edit</button>
		<button onClick="deletePost(${post.id})">Delete</button>
		</div>`

	});
	document.querySelector("#div-post-entries").innerHTML = postEntries;	
}

// add post data
// Making Post request using Fetch: Post requests can be made using fetch by giving options as given below

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),

		headers: {'Content-type': 'application/json; charset=UTF-8 '}
	})
	.then((response) => response.json())
	.then((data) => {
		showPosts();
		// alert("Successfully added");


		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	})

})

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// removeAttribute method removes the "disabled" attribute from the selected element
	document.querySelector("#btn-submit-update").removeAttribute("disabled");

}

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8 '}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		// alert ('Updated')

		document.querySelector("#txt-edit-id").value == null
		document.querySelector("#txt-edit-title").value == null
		document.querySelector("#txt-edit-body").value == null
		document.querySelector("#btn-submit-update").setAttribute("disabled", true);
	})
})

// const deletePost = (id) => {
// 	posts.forEach(p => {
// 	  for (let key in p) {
// 	  	if (p.id == id ){
// 			posts.splice(key, 1);
// 		}
// 		break;
// 	  }
// 	})	

// 	showPosts(posts);
// }

const deletePost = (id) => {

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`{
		method: 'DELETE'
	});

	document.querySelector(`#post-${id}`).remove();
}